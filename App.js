import React, {useState} from 'react';
import {StatusBar, StyleSheet, Text, View, Button, Modal, FlatList, Dimensions, Image, Switch} from 'react-native';
import {Picker} from '@react-native-picker/picker';
import AddGameComponent from "./composants/AddGameComponent";
import HeaderComponent from './composants/HeaderComponent';
import DeleteGameComponent from './composants/DeleteGameComponent';

export default function App() {
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [selectedGameId, setSelectedGameId] = useState(null);
    const [games, setGames] = useState([
        {
            name: "Medal of Honor",
            price: "10€",
            categorie: "FPS",
            id: 23124,
            vignette: 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg',
        },
        {
            name: "Street Fighter 2",
            price: "20€",
            categorie: "Combat",
            id: 12349,
            vignette: 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg',
        },

        {
            name: "Call of Duty",
            price: "30€",
            categorie: "FPS",
            id: 549762,
            vignette: 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg',
        },

        {
            name: "NBA2K5",
            price: "5€",
            categorie: "Sport",
            id: 549763,
            vignette: 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg',
        },

        {
            name: "God Of War 2018",
            price: "25€",
            categorie: "Action-Aventure",
            id: 549764,
            vignette: 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg',
        },

        {
            name: "The Legend of Zelda : The Wind Walker",
            price: "35€",
            categorie: "Action-Aventure",
            id: 549765,
            vignette: 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg',
        },

        {
            name: "Horizon : Forbidden West",
            price: "40€",
            categorie: "Action-Aventure",
            id: 549766,
            vignette: 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg',
        },

        {
            name: "Forza Horizon 5",
            price: "45€",
            categorie: "Voiture",
            id: 549767,
            vignette: 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg',
        },

        {
            name: "The Last Of Us",
            price: "55€",
            categorie: "Survival horror",
            id: 549768,
            vignette: 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg',
        },

        {
            name: "Red Dead Redemption II",
            price: "18€",
            categorie: "Action-Aventure",
            id: 549769,
            vignette: 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg',
        }
    ]);
    const [selectedCategory, setSelectedCategory] = useState("toiAussiRelou");
    const [isSortCroissant, setIsSortCroissant] = useState(false);

    function handleDeleteGame(id) {
        setGames(currentGames => currentGames.filter(game => game.id !== id));
        setIsModalVisible(false);
    }

    const generateUniqueID = () => {
        //sans sa sa peut casser
        let uniqueID = Math.random();
        while (games.find(game => game.id === uniqueID)) {
            uniqueID = Math.random();
        }
        return uniqueID;
    };
    
    const handleAddGame = (newGame) => {
        setGames(currentGames => [...currentGames, {...newGame, id: generateUniqueID()}]);
    };
    function openDeleteModal(id) {
        setSelectedGameId(id);
        setIsModalVisible(true);
    }

    const renderItem = ({item}) => {
        return (
            <View style={[styles.item, sortedGames.length === 1 && styles.relou]}>
                <Text style={styles.name}>{item.name}</Text>
                <Image source={{uri: item.vignette}} style={styles.vignette}/>
                <Text>{item.categorie}</Text>
                <Text>{item.price}</Text>
                <Button title="Supprimer" onPress={() => openDeleteModal(item.id)}/>
            </View>
        );
    };

    const filteredGames = games.filter(game => selectedCategory === "toiAussiRelou" || game.categorie === selectedCategory);
    const sortedGames = [...filteredGames].sort((a, b) => {
        if (isSortCroissant) {
            return a.price.localeCompare(b.price);
        } else {
            return b.price.localeCompare(a.price);
        }
    });

    return (
        <View style={styles.container}>
            <HeaderComponent games={games}/>

            <View style={styles.sortContainer}>
                <Picker
                    selectedValue={selectedCategory}
                    style={{height: 50, width: 150}}
                    onValueChange={(itemValue, itemIndex) => setSelectedCategory(itemValue)}
                >
                    <Picker.Item label="Tout" value="toiAussiRelou"/>
                    <Picker.Item label="FPS" value="FPS"/>
                    <Picker.Item label="Combat" value="Combat"/>
                    <Picker.Item label="Sport" value="Sport"/>
                    <Picker.Item label="Action-Aventure" value="Action-Aventure"/>
                    <Picker.Item label="Voiture" value="Voiture"/>
                    <Picker.Item label="Survival horror" value="Survival horror"/>
                </Picker>

                <View style={styles.switchContainer}>
                    <Text style={styles.label}>Trier par prix croissant</Text>
                    <Switch
                        value={isSortCroissant}
                        onValueChange={setIsSortCroissant}
                    />
                </View>
                <AddGameComponent onAddGame={handleAddGame}/>
            </View>
            <DeleteGameComponent
                isModalVisible={isModalVisible}
                handleDeleteGame={handleDeleteGame}
                selectedGameId={selectedGameId}
                setIsModalVisible={setIsModalVisible}
            />

            <View style={styles.itemContainer}>
                <FlatList
                    style={styles.list}
                    data={sortedGames}
                    renderItem={renderItem}
                    keyExtractor={item => item.id.toString()}
                    numColumns={2}
                />
            </View>
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        marginTop: StatusBar.currentHeight,
    },
    itemContainer: {
        height: Dimensions.get('window').height * 0.5,
        overflow: 'hidden',
    },
    item: {
        width: '50%',
        borderWidth: 1,
        borderColor: '#000',
    },
    name: {
        fontSize: 18,
        fontWeight: 'bold',
    },
    details: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    vignette: {
        width: "fit-content",
        height: 100,
    },
    list: {
        height: 400,
        overflow: 'scroll',
    },
    sortContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        marginBottom: 20,
    },
    checkboxContainer: {
        flexDirection: 'row',
        marginBottom: 20,
    },
    checkbox: {
        alignSelf: 'center',
    },
    label: {
        margin: 8,
    },
    switchContainer: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    relou: {
        width: '100%',
    },
});