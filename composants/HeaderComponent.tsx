import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const HeaderComponent = ({ games }) => {
    return (
        <View style={styles.header}>
            <Text style={styles.text}>Anthony</Text>
            <Text style={styles.text}>Nb jeux vidéo : {games.length}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 20,
        marginVertical: 8,
        marginHorizontal: 16,
        width: 300,
        borderWidth: 1,
        borderColor: '#000',
    },
    text: {
        fontSize: 16,
    },
});

export default HeaderComponent;