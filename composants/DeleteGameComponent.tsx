import React from 'react';
import { Button, Modal, Text, View, StyleSheet } from 'react-native';

const DeleteGameComponent = ({ isModalVisible, handleDeleteGame, selectedGameId, setIsModalVisible }) => {
    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={isModalVisible}
        >
            <View style={styles.centeredView}>
                <View style={styles.modalView}>
                    <Text style={styles.modalText}>tu veux supr?</Text>
                    <Button title="OUI" onPress={() => handleDeleteGame(selectedGameId)}/>
                    <Button title="NON" onPress={() => setIsModalVisible(false)}/>
                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
});

export default DeleteGameComponent;