import React, {useState} from 'react';
import {Button, Modal, Text, TextInput, View, StyleSheet} from 'react-native';
import {launchImageLibrary} from 'react-native-image-picker';
import {Picker} from '@react-native-picker/picker';

const AddGameComponent = ({onAddGame}) => {
    const [isAddModalVisible, setIsAddModalVisible] = useState(false);
    const [newGame, setNewGame] = useState({name: "", categorie: "", price: "", vignette: ""});

    function handleAddGame() {
        if (!newGame.vignette) {
            newGame.vignette = 'https://wallpaper.forfun.com/fetch/cc/cc7b0dd8597c8bf85417a376c98fce3d.jpeg';
        }
        onAddGame(newGame);
        setNewGame({name: "", categorie: "", price: "", vignette: ""});
        setIsAddModalVisible(false);
    }

    function handleSelectImage() {
        //possible unhandled promise rejection ....
        launchImageLibrary({mediaType: 'photo'}, (response) => {
            const source = {uri: response.assets[0].uri};
            setNewGame(currentGame => ({...currentGame, vignette: source.uri}));

        });
    }

    return (
        <>
            <Button title="+" onPress={() => setIsAddModalVisible(true)}/>
            <Modal
                animationType="slide"
                transparent={true}
                visible={isAddModalVisible}
            >
                <View style={styles.centeredView}>
                    <View style={styles.modalView}>
                        <Text style={styles.modalText}>ajouter un jeux</Text>
                        <View style={styles.inputContainer}>
                            <View style={styles.inputFields}>
                                <TextInput
                                    placeholder="nom"
                                    value={newGame.name}
                                    onChangeText={text => setNewGame(currentGame => ({...currentGame, name: text}))}
                                />
                                <Picker
                                    selectedValue={newGame.categorie}
                                    onValueChange={(itemValue) => setNewGame(currentGame => ({
                                        ...currentGame,
                                        categorie: itemValue
                                    }))}
                                >
                                    <Picker.Item label="FPS" value="FPS"/>
                                    <Picker.Item label="Combat" value="Combat"/>
                                    <Picker.Item label="Sport" value="Sport"/>
                                    <Picker.Item label="Action-Aventure" value="Action-Aventure"/>
                                    <Picker.Item label="Voiture" value="Voiture"/>
                                    <Picker.Item label="Survival horror" value="Survival horror"/>
                                </Picker>
                                <TextInput
                                    placeholder="prix"
                                    value={newGame.price}
                                    onChangeText={text => setNewGame(currentGame => ({...currentGame, price: text}))}
                                />
                            </View>
                            <Button title="Select Image" onPress={handleSelectImage}/>
                        </View>
                        <Button title="ajouter" onPress={handleAddGame}/>
                        <Button title="retour" onPress={() => setIsAddModalVisible(false)}/>
                    </View>
                </View>
            </Modal>
        </>
    );
};

const styles = StyleSheet.create({
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    inputContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
    },
    inputFields: {
        flexDirection: 'column',
        width: '70%',
    },
});
export default AddGameComponent;